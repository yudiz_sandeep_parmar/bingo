require('./env');

const socket = require('./socketio/');

const { mongo } = require('./utils/');

socket.init();

mongo.init();

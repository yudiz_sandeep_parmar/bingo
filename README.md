
# Bingo

Small game built using socketIO & redis!


## Prerequisties 

- SocketIO Version (4.0+)
- ioredis
- mongodb
- @socket.io/redis-adapter
- jsonwebtoken
- nanoid

## Description

- Scalable socket server using node cluster module.
- Redis adapter for maintaing nodes!
- Redis hash for caching.
- MongoDB for end result.

## License 

- The project is licensed under the BSD license.

## Contact

-- sailingwithsandeep@gmail.com



module.exports.customMessages = {
  internal_server_error: { code: 500, message: 'Server error!' },
  unauthorized: { code: 401, message: 'Unauthorized!' },
  game_started: { code: 201, message: 'Game started!' },
  game_already_started: { code: 200, message: 'Game already Started!' },
  room_not_found: { code: 404, message: 'Room not found!' },
  user_not_found: { code: 404, message: 'User not found!' },
  user_part_of_other_room: { code: 409, message: 'Already part of other room!' },
  room_full: { code: 419, message: 'Room is full!' },
  join_room: { code: 201, message: 'Joined room successfully!' },
  create_room: { code: 201, message: 'Room created!' },
  game_not_started: { code: 419, message: 'Game not started or room not existed!' },
  no_bingo: { status: 200, message: 'No Bingo!' },
  bingo: { status: 200, message: 'Bingo!' }

};

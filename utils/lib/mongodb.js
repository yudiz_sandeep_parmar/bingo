const { MongoClient } = require('mongodb');

class MongoDbClient {
  constructor() {
    this.client = new MongoClient(process.env.MONGO_URL);
  }

  /**
   * Method for connection of mongodb client
   */
  async init() {
    try {
      await this.client.connect();
      console.log('mongo client connected!');
    } catch (error) {
      console.log(error);
    }
  }

  /**
   *
   * @param {*} participantData
   * save end result information in collection!
   */
  async save(participantData) {
    try {
      const obj = {
        iRoomId: participantData.roomData.iRoomId,
        sRoomName: participantData.roomData.sRoomName,
        aParticipant: participantData.roomData.aParticipant.map((e) => ({ iUserId: e.iUserId, sUserName: e.sUserName, eRole: e.eRole })),
        eState: participantData.roomData.eState,
        iWinnerId: participantData.iUserId,
        dCreatedDate: participantData.roomData.dCreatedDate
      };

      const db = this.client.db(process.env.DB_NAME);
      const collection = db.collection('bingo-logs');
      await collection.insertOne(obj);
    } catch (error) {
      console.log(error);
      return null;
    }
  };

  /**
   *
   * @param {*} iRoomId
   * @returns Returns an object consist of userData!
   */

  async getData(iRoomId) {
    try {
      const db = this.client.db(process.env.DB_NAME);
      const collection = db.collection('bingo-logs');
      const data = await collection.findOne({ iRoomId });
      if (Object.keys(data).length === 0) return null;
      return data;
    } catch (error) {
      console.log(error);
      return null;
    }
  }

  async saveScore(iRoomId) {
    try {
      const db = this.client.db(process.env.DB_NAME);

      const rankCollection = db.collection('ranks');
      const roomCollection = db.collection('bingo-logs');
      const data = await roomCollection.findOne({ iRoomId });

      const userData = data.aParticipant.find((e) => e.iUserId === data.iWinnerId);
      const rankedData = await rankCollection.findOne({ iUserId: userData.iUserId });
      if (!rankedData) {
        userData.score = 1;
        await rankCollection.insertOne(userData);
      } else {
        await rankCollection.updateOne({ iUserId: rankedData.iUserId }, { $inc: { score: 1 } });
      }
    } catch (error) {
      console.error(error);
      return null;
    }
  }

  async getScore() {
    try {
      const db = this.client.db(process.env.DB_NAME);
      const rankCollection = db.collection('ranks');
      const data = rankCollection.find().sort({ score: 1 });
      const doc = await data.toArray();
      return doc;
    } catch (error) {
      console.error(error);
      return null;
    }
  }
}

module.exports = new MongoDbClient();

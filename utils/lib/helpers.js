/* eslint-disable no-prototype-builtins */

const { customAlphabet } = require('nanoid');
const roomIdGenerator = customAlphabet(process.env.ID_SECRET, 6);
const userIdGenerator = customAlphabet(process.env.ID_SECRET, 10);
const jwt = require('jsonwebtoken');

const _ = {};

_.parse = function(data) {
  try {
    return JSON.parse(data);
  } catch (error) {
    return data;
  }
};

_.stringify = function(data, offset = 0) {
  return JSON.stringify(data, null, offset);
};

_.generateRoomId = () => {
  try {
    return roomIdGenerator();
  } catch (error) {
    console.error();
    return error;
  }
};

_.generateUserId = () => {
  try {
    return userIdGenerator();
  } catch (error) {
    console.error();
    return error;
  }
};

_.generateAuthToken = (iUserId) => {
  try {
    return jwt.sign({ _id: iUserId?.toString() }, process.env.TOKEN);
  } catch (error) {
    return undefined;
  }
};

_.verifyToken = function(token) {
  try {
    return jwt.verify(token, process.env.TOKEN, function(err, decoded) {
      return err ? err.message : decoded; // return true if token expired
    });
  } catch (error) {
    return error ? error.message : error;
  }
};

// generate message for client socket!
_.generateMessage = (username, text) => {
  return {
    username,
    text,
    createdAt: new Date().getTime()
  };
};

_.getFewProperty = (object, keys) => {
  console.log(keys);
  return keys.reduce((obj, key) => {
    if (object && object.hasOwnProperty(key)) {
      obj[key] = object[key];
    }
    return obj;
  }, {});
};

module.exports = _;

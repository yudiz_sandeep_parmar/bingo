const Cluster = require('ioredis');

const option = {
  host: process.env.REDIS_HOST,
  port: process.env.REDIS_PORT,
  username: process.env.REDIS_USERNAME,
  password: process.env.REDIS_PASSWORD
};

const redisClient = new Cluster(option);
const pubClient = new Cluster(option);
const subClient = pubClient.duplicate();

redisClient.on('error', function(error) {
  console.log('Error in Redis', error);
  process.exit(1);
});

redisClient.on('connect', function() {
  console.log('redis client connected!');
});

module.exports.pubClient = pubClient;
module.exports.subClient = subClient;
module.exports.redisClient = redisClient;

How to scale nodejs server?
We can achieve load balancing using adapters most probably with redis we can implement public subcribers which can handle events but for now we have used nodejs cluster module for initial phase

Why use redis?
we are using redis as a cache database and for better performance and later we can dump into mongodb & we can set expiry of each key-value

Which data structure you used?
we used hash data structure because our schema was object type and with redis hash it was easy to maintain also, we can use redisJSON module too and we are implementing it future versions

How you handled if two users join the room at the same time?
We have created a counter in redis so at each call we are checking that room is full or not after that we are doing necessary actions and we are decreasing the counter if use does not meet the requirements

How can you handle reconnection if user leaves in middle of game?
we are checking on each room join event that if user is in any room and state of room then user will immediately join the that particular room

What is bingo?
bingo is a game where each user provided with unique board consist of numbers and a random number will appear on screen if the number matches then user can check and see if bingo happens or not basic rules are it should raw, column and diagonal.

How you are checking that user has bingo or not?
We are providing 5 x 5 array to each user that consist with zero and then if any number matches we are replacing that zero with one and checking pattern of one's.

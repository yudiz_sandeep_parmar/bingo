const { redis, _ } = require('../../utils');
const Game = require('./lib/Game');

class GameManager {
  /**
   *
   * @param {*} iRoomId
   * @returns get instance of Game class with roomData from redis!
   */
  async getRoomData(iRoomId) {
    try {
      const room = await redis.redisClient.hgetall('room:' + iRoomId);
      if (Object.keys(room).length === 0) return null;
      const _object = {};
      for (const [_key, _value] of Object.entries(room)) {
        if (_key.includes('aParticipant')) {
          _object.aParticipant = _object.aParticipant || [];
          _object.aParticipant.push(_.parse(_value));
        } else if (_value) _object[_key] = _value;
      }

      return new Game(_object);
    } catch (error) {
      console.error(error);
    }
  }

  /**
   *
   * @param {*} oUserData
   * save userData in redis hash!
   */
  async saveUser(oUserData) {
    try {
      const obj = {
        iUserId: oUserData.iUserId,
        sToken: oUserData.sToken
      };
      await redis.redisClient.hmset('user:' + obj.iUserId, obj);
      await redis.redisClient.expire('user:' + obj.iUserId, 129600);
    } catch (error) {
      console.error(error);
    }
  }

  /**
   *
   * @param {*} oUserData
   * save Bingo of each user in redis hash!
   */

  async saveUserBingo(oUserData) {
    try {
      const obj = {
        iUserId: oUserData.iUserId,
        aBingoCard: oUserData?.aBingoCard

      };
      if (typeof oUserData.aBingoCard !== 'string') {
        obj.aBingoCard = _.stringify(oUserData?.aBingoCard);
        await redis.redisClient.hmset('bingo:' + obj.iUserId, obj);
        await redis.redisClient.expire(':' + obj.iUserId, 129600);
      }
      await redis.redisClient.hmset('bingo:' + obj.iUserId, obj);
      await redis.redisClient.expire('bingo:' + obj.iUserId, 129600);
    } catch (error) {
      console.error(error);
    }
  }

  /**
   *
   * @param {*} iUserId
   * @returns get specified user's bingo!
   */
  async getUserBingo(iUserId) {
    try {
      const user = await redis.redisClient.hgetall('bingo:' + iUserId);

      if (Object.keys(user).length === 0) return null;
      const _object = {};
      for (const [_key, _value] of Object.entries(user)) {
        if (_value) _object[_key] = _value;
      }

      return _object;
    } catch (error) {
      console.error(error);
    }
  }
}

module.exports = new GameManager();

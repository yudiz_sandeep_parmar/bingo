class Participant {
  constructor(body, game) {
    this.iUserId = body.iUserId;
    this.sUserName = body.sUserName;
    this.sToken = body.sToken;
    this.eRole = body.eRole;
    this.game = game;
    this.aBingoCard = body?.aBingoCard || [];
  }

  async emit(channel, data) {
    const socket = await global.io.to(this.game.iRoomId).fetchSockets();
    global.io.to(socket).emit(channel, data);
  }

  toJSON() {
    return {
      iUserId: this.iUserId,
      sUserName: this.sUserName,
      sToken: this.sToken,
      eRole: this.eRole,
      aBingoCard: this.aBingoCard
    };
  }
}

module.exports = Participant;

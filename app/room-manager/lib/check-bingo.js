// check if bingo happens or not!
function isBingo(userBingo) {
  if (Object.keys(userBingo).length === 0) return { code: 500, message: 'Internal Server Error!' };
  return checkRow(userBingo) || checkColumn(userBingo) || checkLeftDiagonal(userBingo) || checkRightDiagonal(userBingo);
}

// check row based array
function checkRow(userBingo) {
  let winner = true;
  for (let i = 0; i < userBingo.aBingoCard.length; i++) {
    for (let j = 0; j < userBingo.aBingoCard[i].length; j++) {
      if (userBingo.aBingoCard[i][j] !== 1) {
        winner = false;
        break;
      }
    }
    if (winner) {
      return true;
    }
  }
  return false;
}

// check column based array
function checkColumn(userBingo) {
  const sum = userBingo.aBingoCard.reduce(function(array1, array2) {
    return array1.map(function(value, index) {
      return value + array2[index];
    });
  });
  if (sum.includes(5)) {
    return true;
  }
  return false;
}

// check left diagonal based array
function checkLeftDiagonal(userBingo) {
  let count = 0;
  for (let i = 0; i < userBingo.aBingoCard.length; i++) {
    for (let j = 0; j < userBingo.aBingoCard[i].length; j++) {
      if (i === j) {
        if (userBingo.aBingoCard[i][j] === 1) {
          count++;
        };
      }
    }
  }
  return count === 5;
}

// check right diagonal based array
function checkRightDiagonal(userBingo) {
  let count = 0;
  for (let i = 0; i < userBingo.aBingoCard.length; i++) {
    for (let j = userBingo.aBingoCard[i].length; j >= 0; j--) {
      if (userBingo.aBingoCard[i][j] === 1) {
        count++;
      };
    }
  }

  return count === 5;
}
module.exports = isBingo;

const Participant = require('./Participant');
const { _, redis } = require('../../../utils');

class Game {
  constructor(body) {
    this.iRoomId = body.iRoomId;
    this.sRoomName = body.sRoomName;
    this.aParticipant = body?.aParticipant?.map(p =>
      new Participant(p, this) || []
    );
    this.eState = body.eState;
    this.dCreatedDate = body.dCreatedDate;
    this.socket = {};
  }

  async addParticipant(participantData) {
    this.aParticipant.push(participantData);
    return this.aParticipant;
  }

  emit(channel, data) {
    global.io.in(this.iRoomId).emit(channel, data);
  }

  /**
   *
   * @param {*} roomData
   * method to save roomData in redis with hash!
   */
  async save(data = this.toJSON()) {
    try {
      const _object = {};
      const stringParseKeys = ['dCreatedDate'];

      for (const [_key, _value] of Object.entries(data)) {
        if (_key === 'aParticipant') _value.map(v => (_object[`aParticipant:${v.iUserId}`] = _.stringify(v)));
        else if (stringParseKeys.includes(_key) && _value) _object[_key] = _value.toString();
        else if (_value) _object[_key] = _value;
      }

      await redis.redisClient.hmset(`room:${_object.iRoomId}`, _object);
      await redis.redisClient.expire(`room:${_object.iRoomId}`, 129600);
    } catch (error) {
      console.error(error);
    }
  }

  // delete room from redis
  async deleteRoom(iRoomId) {
    try {
      const room = await this.getRoomData(iRoomId);
      if (Object.keys(room).length === 0) return null;

      if (room.eState === 'GAME_OVER') {
        redis.redisClient.del('room:' + iRoomId);
      }
    } catch (error) {
      console.error(error);
    }
  }

  toJSON() {
    return {
      iRoomId: this.iRoomId,
      sRoomName: this.sRoomName,
      aParticipant: this.aParticipant.map(p => new Participant(p, this)) || [],
      eState: this.eState,
      dCreatedDate: this.dCreatedDate
    };
  }
}

module.exports = Game;

class Card {
  /**
     * @param {number} min
     * @param {number} max
     * @returns array
     *
     * Method to create array of numbers
     */
  createNumbers(min = 1, max = 100) {
    return Array.from({ length: max - min + 1 })
      .map((_, index) => index + min);
  }

  /**
   *
   * @param {*} availableNumbers
   * @returns next element
   */
  selectNextNumber(availableNumbers) {
    const index = Math.round(Math.random() * (availableNumbers.length - 1));
    return availableNumbers[index];
  }

  /**
   *
   * @param {*} availableNumbers
   * method to select numbers to avoid duplication!
   */

  * selectNumbers(availableNumbers = this.createNumbers()) {
    const numbers = [...availableNumbers];
    while (numbers.length) {
      const next = this.selectNextNumber(numbers);
      const index = numbers.indexOf(next);
      numbers.splice(index, 1);
      yield { next, remaining: [...numbers] };
    }
  }

  /**
   *
   * @param {*} currentLines
   * @param {*} line
   * check if line is valid or not!
   */
  isLineValid(currentLines, line) {
    const sorted = [...line].sort();

    const foundIndex = currentLines.findIndex(
      currentLine => {
        if (currentLine.length !== sorted.length) {
          return false;
        }
        const currentLineSorted = [...currentLine].sort();

        const mismatchIndex = sorted.findIndex(
          (value, index) => currentLineSorted[index] !== value
        );
        return mismatchIndex === -1;
      }
    );
    return foundIndex === -1;
  }

  /**
   * @param {*} currentLines
   * @param {*} generator
   * @param {*} length
   * @returns recursive function to select lines from array
   */

  selectLine(currentLines, generator, length) {
    const line = [];
    let remaining;
    while (line.length < length) {
      const { value, done } = generator.next();
      if (done) {
        return undefined;
      }
      const { next, remaining: currentRemaining } = value;
      remaining = currentRemaining;
      line.push(next);
    }
    if (this.isLineValid(currentLines, line)) {
      return line;
    }
    if (line.length === remaining.length) {
      throw new Error("We've created a set of lines where we will always get a duplicate!");
    }

    return this.selectLine(
      currentLines,
      this.selectNumbers(
        [
          ...remaining,
          ...line
        ]
          .sort()
      ),
      length
    );
  }

  /**
 *
 * @param {*} currentCards
 * @param {*} numbers
 * @returns 5*5 matrix
 */

  createCard(currentCards, numbers = this.createNumbers()) {
    const currentCardLines = currentCards.reduce(
      (lines, card) => lines.concat(card), []
    );
    const card = [];
    const generator = this.selectNumbers(numbers);
    while (card.length < 5) {
      const line = this.selectLine([

        ...currentCardLines,
        ...card
      ], generator, 5);
      if (!line) {
        return undefined;
      }
      card.push(line);
    }
    return card;
  }

  /**
   *
   * @param {*} amount
   * @param {*} numbers
   * @returns array of bingo based amount
   */
  createCards(amount, numbers = this.createNumbers()) {
    const cards = [];
    while (cards.length < amount) {
      const card = this.createCard(cards, numbers);
      if (!card) {
        break;
      }
      cards.push(card);
    }
    return cards;
  }

  /**
 *
 * @param {*} array
 * @returns randomized array
 */
  shuffle(array) {
    let currentIndex = array.length;
    let randomIndex;

    while (currentIndex !== 0) {
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex--;

      [array[currentIndex], array[randomIndex]] = [
        array[randomIndex], array[currentIndex]];
    }

    return array;
  }
}

module.exports = new Card();

const { redis, constants, _, mongo, message } = require('../utils');
const gameManager = require('./room-manager');
const Game = require('./room-manager/lib/Game');
const card = require('./room-manager/lib/Card');
const isBingo = require('./room-manager/lib/check-bingo');

class Socket {
  /** A constructor to use socket inside class methods and to register events! */
  constructor(socket) {
    this.socket = socket;
    this.setEventListener();
    console.log(`${this.socket.id} connected.`);
  }

  /**
   * Method to register events!
   */
  setEventListener() {
    this.socket.on('join-room', this.joinRoom.bind(this));
    this.socket.on('create-room', this.createRoom.bind(this));
    this.socket.on('send-cards', this.sendCards.bind(this));
    this.socket.on('check-bingo', this.checkBingo.bind(this));
    this.socket.on('score', this.getRanks.bind(this));
    this.socket.on('disconnect', this.disconnect.bind(this));
  }

  /**
   *
   * @param {*} body
   * @param {*} callback
   *
   * Method to join room for user!
   */
  async joinRoom(body, callback) {
    try {
      const user = await redis.redisClient.hgetall(`user:${this.socket.user.iUserId}`);
      if (Object.keys(user).length === 0) return callback(message.customMessages.user_not_found, {});
      if (!this.socket?.user) return callback(message.customMessages.unauthorized, {});

      const room = await gameManager.getRoomData(body.iRoomId);
      if (!room) return callback(message.customMessages.room_not_found, { data: { iRoomId: room.iRoomId } });

      if (room.eState === 'IN_GAME') {
        return callback(message.customMessages.game_already_started, {});
      }

      if (room.iRoomId) {
        if (room.iRoomId === body.iRoomId) {
          this.socket.join(body.iRoomId);
          callback(message.customMessages.join_room, { data: room.toJSON() });
        } else callback(message.customMessages.user_part_of_other_room, { data: { iRoomId: body.iRoomId } });
      }

      const userObj = {
        iUserId: this.socket.user.iUserId, sUserName: body.sUserName, eRole: constants.eState.MEMBER, sToken: this.socket.user.token
      };

      if (room.aParticipant?.length > 5) return callback(message.customMessages.room_full, { iRoomId: room.iRoomId });

      const aParticipant = await room.addParticipant(userObj);
      await room.save({ iRoomId: body.iRoomId, aParticipant });

      this.socket.join(body.iRoomId);
      global.io.in(body.iRoomId).emit('response-room-data', room.toJSON());

      return callback(message.customMessages.join_room, { data: room.toJSON() });
    } catch (error) {
      console.log(error);
      return callback(message.customMessages.internal_server_error);
    }
  }

  /**
   *
   * @param {*} body
   * @param {*} callback
   * Method to create room for host!
   */
  async createRoom(body, callback) {
    try {
      const generatedRoomId = _.generateRoomId();
      const roomData = {
        iRoomId: generatedRoomId,
        sRoomName: body.sRoomName,
        eState: constants.eState.IN_LOBBY,
        dCreatedDate: body.dCreatedDate,
        aParticipant: []
      };
      const participantData = {
        iUserId: this.socket.user.iUserId,
        sUserName: body.sUserName,
        eRole: body.eRole,
        sToken: this.socket.user.token
      };
      const game = new Game(roomData);

      await game.save(roomData);
      const participant = await game.addParticipant(participantData);

      this.socket.join(body.roomId);
      await game.save({ iRoomId: generatedRoomId, aParticipant: participant });

      global.io.in(body.iRoomId).emit('response-room-data', game.toJSON());
      this.socket.to(body.iRoomId).emit('message', _.generateMessage('notification', `${body.username} has joined!`));

      return callback(message.customMessages.create_room);
    } catch (error) {
      console.log(error);
      return callback(message.customMessages.internal_server_error);
    }
  }

  /**
   *
   * @param {*} body
   * @param {*} callback
   * Method to send cards to each user before game starts!
   */
  async sendCards(body, callback) {
    try {
      const lobbyData = await gameManager.getRoomData(body.iRoomId);
      if (!lobbyData) {
        return callback(message.customMessages.room_not_found);
      }
      if (lobbyData.aParticipant.length > 1) {
        this.socket.emit('start-game');

        const playersLength = lobbyData.aParticipant.length;

        const result = card.createCards(playersLength);
        for (let index = 0; index < result.length; index++) {
          lobbyData.aParticipant[index].aBingoCard = _.stringify(result[index]);
          await gameManager.saveUserBingo({ iUserId: lobbyData.aParticipant[index].iUserId, aBingoCard: [[0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0]] });
        }

        await lobbyData.save({ iRoomId: body.iRoomId, eState: constants.eState.IN_GAME, aParticipant: lobbyData.aParticipant.map((e) => e.toJSON()) });

        const data = lobbyData.aParticipant.map((e) => ({ iUserId: e.iUserId, sUserName: e.sUserName, eRole: e.eRole, sToken: e.sToken, aBingoCard: _.parse(e?.aBingoCard) || [] }));

        const parsedObj = {
          iRoomId: lobbyData.iRoomId,
          sRoomName: lobbyData.sRoomName,
          eState: lobbyData.eState,
          dCreatedDate: lobbyData.dCreatedDate,
          aParticipant: data
        };

        const randomNumber = card.createNumbers();
        const shuffled = card.shuffle(randomNumber);

        this.socket.emit('send-num', shuffled);
        lobbyData.emit('response-room-data', parsedObj);
      } else { this.socket.to(lobbyData.roomId).emit('message', _.generateMessage('notification', 'you need at least two players!')); }

      return callback(message.customMessages.game_started);
    } catch (error) {
      console.log(error);
      return callback(message.customMessages.internal_server_error);
    }
  }

  /**
   *
   * @param {*} body
   * @param {*} callback
   * Method to check if user has done bingo or not!
   */

  async checkBingo(body, callback) {
    try {
      if (body?.eState !== 'IN_GAME') {
        return callback(message.customMessages.game_not_started);
      }
      const userBingo = await gameManager.getUserBingo(body.iUserId);
      if (!userBingo) {
        return callback(message.customMessages.internal_server_error);
      }

      userBingo.aBingoCard = _.parse(userBingo.aBingoCard);
      const room = await gameManager.getRoomData(body.iRoomId);
      const roomBingo = room.aParticipant.find((e) => e.iUserId === body.iUserId);
      roomBingo.aBingoCard = _.parse(roomBingo.aBingoCard);

      if (userBingo.iUserId === roomBingo.iUserId) {
        if (roomBingo.aBingoCard[body.arrayIndex].includes(body.number)) {
          const innerIndex = roomBingo.aBingoCard[body.arrayIndex].indexOf(body.number);
          userBingo.aBingoCard[body.arrayIndex].splice(innerIndex, 1, 1);
          await gameManager.saveUserBingo({ iUserId: roomBingo.iUserId, aBingoCard: userBingo.aBingoCard });
        }
      }

      const result = isBingo(userBingo, roomBingo.iUserId);
      if (!result) {
        return callback(message.customMessages.no_bingo);
      }

      await room.save({ iRoomId: body.iRoomId, eState: constants.eState.GAME_OVER, aParticipant: room.aParticipant.map((e) => e.toJSON()) });
      room.emit('response-bingo-result', { winner: userBingo.iUserId, result });
      await mongo.save({ roomData: room.toJSON(), iUserId: userBingo.iUserId });
      await mongo.saveScore(room.iRoomId);
      return callback(message.customMessages.bingo, { winner: userBingo.iUserId, result });
    } catch (error) {
      console.log(error);
      return callback(message.customMessages.internal_server_error);
    }
  }

  async getRanks(callback) {
    try {
      const data = await mongo.getScore();
      this.socket.emit('send-ranks', data);
    } catch (error) {
      console.error(error);
      return callback(message.customMessages.internal_server_error);
    }
  }

  /**
   *
   * @param {*} reason
   * Method for disconnected socket connections!
   */
  disconnect(reason) {
    console.log(reason);
    console.log(`${this.socket.id} disconnected.`);
  }
}

module.exports = Socket;

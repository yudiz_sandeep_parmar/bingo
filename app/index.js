const io = require('socket.io');
const http = require('http');
const cluster = require('cluster');
const process = require('process');
const { createAdapter } = require('@socket.io/redis-adapter');
const Socket = require('./Socket');
const { redis, _, message } = require('../utils');
const gameManager = require('./room-manager');

const numOfCPUs = 2;

class SocketBase {
  /**
   * initialize socket server with clustering and redis adapter!
   */
  init() {
    try {
      if (cluster.isPrimary) {
        console.log(`Primary ${process.pid} is running!`);

        // Fork workers.
        for (let i = 0; i < numOfCPUs; i++) {
          cluster.fork();
        }

        cluster.on('exit', (worker, code, signal) => {
          console.log(`worker ${worker.process.pid} died!`);
        });
      } else {
        console.log(`Worker ${process.pid} started!`);
        const server = http.createServer(async(req, res) => {
          const headers = {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'OPTIONS, GET'
          };
          const iUserId = _.generateUserId();
          const sToken = _.generateAuthToken(iUserId);
          const data = { iUserId, sToken };
          await gameManager.saveUser(data);
          res.writeHead(201, headers);
          return res.end(_.stringify(data));
        });
        global.io = io(server, {
          cors: {
            credentials: true
          }
        });
        global.io.adapter(createAdapter(redis.pubClient, redis.subClient));
        server.listen(process.env.PORT, () => {
          console.log(`Magic happens on: ${process.env.PORT}!`);
        });
        this.setEventListener();
      }
    } catch (error) {
      console.error(error);
    }
  }

  /**
 * setting up socket events for initial connection and error handing!
 */
  setEventListener() {
    global.io.use((socket, next) => this.middleware(socket, next));
    global.io.on('connection', (socket) => new Socket(socket));
    global.io.on('connection_error', (error) => console.log('connection_error', error));
  }

  /**
   *
   * @param {*} socket
   * @param {*} next
   * @returns  returns error if connection is not authorized!
   *
   * Middleware method for checking handshake is valid or not!
   */
  async middleware(socket, next) {
    try {
      if (socket.handshake.auth.token) {
        socket.user = {};
        socket.decoded = _.verifyToken(socket.handshake.auth.token);
        socket.user.iUserId = socket.decoded._id;
        socket.user.token = socket.handshake.auth.token;
        const data = await redis.redisClient.hgetall('user:' + socket.decoded._id);
        if (data) {
          await gameManager.saveUser(socket.user);
        }
        if (!socket.user.iUserId === socket.decoded._id) {
          throw new Error(message.customMessages.unauthorized);
        }
        next();
      }
      if (socket.handshake.headers.token) {
        socket.user = {};
        socket.decoded = _.verifyToken(socket.handshake.headers.token);
        socket.user.iUserId = socket.decoded._id;
        socket.user.token = socket.handshake.headers.token;

        const data = await redis.redisClient.hgetall('user:' + socket.decoded._id);
        if (data) {
          await gameManager.saveUser(socket.user);
        }
        if (!socket.user.iUserId === socket.decoded._id) {
          throw new Error(message.customMessages.unauthorized);
        }
        next();
      }
    } catch (error) {
      console.log(error);
      return message.customMessages.unauthorized;
    }
  }
}

module.exports = new SocketBase();
